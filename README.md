This file system is base on busybox-1.15.2 version, compiled with gcc
version 4.3.2 (Sourcery G++ Lite 2008q3-72)

device node in /dev directory is manipulated by udev


please copy the library files at:
/usr/local/arm/4.3.2/arm-none-linux-gnueabi/libc/armv4t/lib directory, the
library files at /usr/local/arm/4.3.2/arm-none-linux-gnueabi/libc/lib may
cause kernel panic like the following:
```
Freeing init memory: 112K
Kernel panic - not syncing: Attempted to kill init!
```

You should have node `console` and `null` in the dev directory otherwise it
will prompt a warning:
```
Warning: unable to open an initial console.
```

So do the following to create the console and null node manully:
```sh
mknod -m 660 null c 1 3
mknod -m 660 console c 5 1
```
